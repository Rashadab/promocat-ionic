import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private storage: Storage, private router: Router ){ }

  canActivate(): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>{
    return this.storage.get('tutorialComplete').then(data => {
      if(!data){
        this.router.navigateByUrl('/welcome');
        return false;
      }
      if(!data.isComplete){
        this.router.navigateByUrl('/welcome');
        return false;
      }

      return true;
    });
  }
}
