import { Injectable } from '@angular/core';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free/ngx';

@Injectable({
  providedIn: 'root'
})
export class AdmobService {

  constructor(
    private adMob: AdMobFree,
    ) { }


// method to show interstitial
showInterstitial() {
  const interstialConfig: AdMobFreeInterstitialConfig  = {
    id: 'ca-app-pub-4759351297830879/1162805912',
    autoShow: true,
   };
  this.adMob.interstitial.config(interstialConfig);
  this.adMob.interstitial.prepare().then(() => {
   console.log('success');
  }).catch(err => {
   console.log(err);
  });
 }

 // Show Banner ads
 showBanner() {
  // banner
  const bannerConfig: AdMobFreeBannerConfig = {
    id: 'ca-app-pub-4759351297830879/3840359589',
    autoShow: true,
    bannerAtTop: false,
    overlap: true,
    offsetTopBar:true,
    };
  this.adMob.banner.config(bannerConfig);
  this.adMob.banner.prepare().then(() => {
                console.log('success');
                }).catch(err => {
                  console.log(err);
                });
  }

    // hide banner
  hideBanner() {
    this.adMob.banner.hide().then(() => {
                    console.log('success');
                    }).catch(err => {
                      console.log(err);
                    });
    }
}
