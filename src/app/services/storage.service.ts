import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Catalog } from '../modals/Catalog';
import { rejects } from 'assert';
const LIKED_CATALOGS = 'liked-catalogs';



@Injectable({
  providedIn: 'root'
})
export class StorageService {
public countryName : string;
  constructor(private storage: Storage) { }

   // ADD A CATALOG
   likeCatalog(catalog: Catalog): Promise<any> {
    return this.storage.get(LIKED_CATALOGS).then((cats: Catalog[]) => {
      if (cats) {
        cats.push(catalog);
        return this.storage.set(LIKED_CATALOGS, cats);
      } else {
        return this.storage.set(LIKED_CATALOGS, [catalog]);
      }
    });
  }

    // DELETE CATALOG BY ID FROM STORAGE
    deleteCatalog(id: any): Promise<Catalog> {
      return this.storage.get(LIKED_CATALOGS).then((cats: Catalog[]) => {
        if (!cats || cats.length === 0) {
          return null;
        }
        const toKeep: Catalog[] = [];
        for (const i of cats) {
          if (i.ID !== id) {
            toKeep.push(i);
          }
        }
        return this.storage.set(LIKED_CATALOGS, toKeep);
      });
    }
 // DELETE CATALOG BY NAME FROM STORAGE
 deleteCatalogByName(name: any): Promise<Catalog> {
  return this.storage.get(LIKED_CATALOGS).then((cats: Catalog[]) => {
    if (!cats || cats.length === 0) {
      return null;
    }
    // tslint:disable-next-line: prefer-const
    let toKeep: Catalog[] = [];
    for (let i of cats) {
      if (i.Name !== name) {
        toKeep.push(i);
      }
    }
    return this.storage.set(LIKED_CATALOGS, toKeep);
  });
}

  // READ CATALOGS FROM STORE
  getItems(): Promise<Catalog[]> {
    return new Promise(resolve => {
      this.storage.get(LIKED_CATALOGS).then(data => {
        if (data) {
          resolve(data);
        } else {
          this.storage.set(LIKED_CATALOGS, []);
        }
      });
    });
  }
 // CLEAR ALL FAVORITES FROM STORAGE
 clearFavorites(): Promise<any> {
  return this.storage.clear();
}


  // get the country select catalog
  async getCountrySelection(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if(result != null){
        this.countryName = result.country;
        return result;
      }else {
        return null;
      }
    }catch(reason){
        console.log(reason);
        return null;
    }
  }


}
