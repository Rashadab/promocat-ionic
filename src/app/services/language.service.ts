import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor( private storage: Storage, private translate: TranslateService) {

   }
setIntialLanguage() {
this.storage.get('tutorialComplete').then( a => {
  if (a && a.lenguaje) {
    this.translate.use(a.lenguaje);
  } else {
    this.translate.use('es');
  }
});
}

translateSpanish() {
  this.translate.use('es');
}

translateEnglish() {
  this.translate.use('en');
}
}
