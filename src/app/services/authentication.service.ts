import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
authState: any = null;
userId: any;
  constructor(private afAuth: AngularFireAuth) {
    this.authSubscribe();
   }

   authSubscribe(){
       this.afAuth.authState.subscribe( auth => {
         if(auth){
           this.authState = auth;
           this.userId = auth.uid;
           console.log('User Id: ',this.userId);
         }
       })
   }

   // Sign in anonymously
   signInAnonymously(){
     this.afAuth.signInAnonymously().catch(error => {
      const errorCode = error.errorCode;
      const errorMessage = error.errorMessage;
      console.log(errorCode + errorMessage); 
     });
   }
}
