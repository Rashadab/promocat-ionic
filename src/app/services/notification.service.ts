import { Injectable } from '@angular/core';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private firebase: FirebaseX) { }

  // Subscribe to a topic
sub(topic) {
  this.firebase.subscribe(topic).then((res: any) => {
  }).catch(e =>{
  });
  }
  // Unsubscribe from a topic
  unsub(topic) {
   this.firebase.unsubscribe(topic).then((res: any) => {
  }).catch(e => {
  });
  }

}
