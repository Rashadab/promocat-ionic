import { Injectable, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Catalog } from '../modals/Catalog';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';


@Injectable({
  providedIn: 'root'
})
export class CatalogService  implements OnInit{
  db = firebase.firestore();
   catalogs = [];
  fecha = new Date().setHours(0, 0, 0, 0);
  todaysDate = new Date(this.fecha);
  allCats: any;
  public allCatalogs: Catalog[] = [];
  public supermarketCatalogs: Catalog[] = [];
  public beautyCatalogs: Catalog[] = [];
  public fashionCatalogs: Catalog[] = [];
  public travelCatalogs: Catalog[] = [];
  public electronicCatalogs: Catalog[] = [];
  public furnitureCatalogs: Catalog[] = [];
  public otherCatalogs: Catalog[] = [];
  public favoriteCatalogs: Catalog[] = [];
  public popularCatalogs: Catalog[] = [];
  public likedCatalogs = [];
  constructor(private afs: AngularFirestore) { }
  ngOnInit(){
  }

  getCatalogs(country: string) {
    console.log('.....querrying');
    this.catalogs = [];
    const reference = this.afs.collection('Country').doc(country).collection('Catalogs');
    const querry1 = reference.ref.where('EndDate', '>=', this.todaysDate);
    return querry1.get().then(snapShot => {
      snapShot.forEach(cat => {
        this.catalogs.push({ ...cat.data()});
      });
      return this.catalogs;
    });
  }


async loadCatalogs(country?: string){
const  ev = new Event('catalogsLoaded')
this.allCats= await this.getCatalogs(country);
this.sortCatalogs(this.allCats)
this.supermarketCatalogs = this.allCats.filter(a => a.Category === 'Supermarket');
this.beautyCatalogs =  this.allCats.filter(a => a.Category === 'Beauty');
this.fashionCatalogs = this.allCats.filter(a => a.Category === 'Fashion');
this.travelCatalogs = this.allCats.filter(a => a.Category === 'Travel');
this.otherCatalogs = this.allCats.filter(a => a.Category === 'Other');
this.electronicCatalogs = this.allCats.filter(a => a.Category === 'Electronic');
this.furnitureCatalogs = this.allCats.filter(a => a.Category === 'Furniture');
this.popularCatalogs = this.allCats.filter(a => a.Likes >= 1);
window.dispatchEvent(ev);
}


sortCatalogs(catPromise: any){
  catPromise.sort((a, b) => Date.parse(b.publishDateString) - Date.parse(a.publishDateString));
}


  // incremenet like
  incrementLikes(country: string, id: string) {
   const  increment = firebase.firestore.FieldValue.increment(1);
    const catRef = this.db.collection('Country').doc(country).collection('Catalogs').doc(id);
    catRef.update({ Likes: increment });
  }

  // decrement likes
  decrementLikes(country: string, id:string) {
    const decrement = firebase.firestore.FieldValue.increment(-1);
    const catRef = this.db.collection('Country').doc( country).collection('Catalogs').doc(id);
    catRef.update({ Likes: decrement });

}

  decrementLikesByName(item: any, catalogs: Catalog[]){
    const decrement = firebase.firestore.FieldValue.increment(-1);
    catalogs.forEach(cat=> {
      if(cat.Name === item.Name){
        const catRef = this.db.collection('Country').doc(cat.Country).collection('Catalogs').doc(cat.ID);
        catRef.update({ Likes: decrement });
      }

    });
  }


}
