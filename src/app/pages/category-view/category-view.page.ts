import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog.service';
import { ModalController } from '@ionic/angular';
import { Catalog } from 'src/app/modals/Catalog';
import { CatalogViewPage } from '../catalog-view/catalog-view.page';
import { StorageService } from 'src/app/services/storage.service';
import { AdmobService } from 'src/app/services/admob.service';

@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.page.html',
  styleUrls: ['./category-view.page.scss'],
})
export class CategoryViewPage implements OnInit {
  name: string;
  arrayCategory: Array<any>;
  transName: string;
  favCatalogs: Catalog[] = [];
  storedLikeSate: string;
  date = new Date();
  showNew = false;
  dateToday = new Date(this.date.getTime()).setHours(0,0,0,0);


   // favorite Catalog Promise
   favoriteCatalogsPromise: any;

  constructor(private catalogService: CatalogService,
              private modalController: ModalController,
              private sService: StorageService,
              private admobService: AdmobService) { }

ngOnInit() {
this.displayCategory(this.name);
this.loadStoredItems();
this.admobService.showBanner();

}
ionViewDidLeave(){
  this.admobService.hideBanner();
}

displayCategory(name){
  let categoryArray: Array<any>;
  switch (name) {
    case 'Supermercado':
    case 'Supermarket':
     categoryArray = this.catalogService.supermarketCatalogs;
     break;
      case 'Moda':
      case 'Fashion':
      categoryArray = this.catalogService.fashionCatalogs;
      break;
      case 'Electronics':
        case 'Electrónica':
          categoryArray = this.catalogService.electronicCatalogs;
          break;
          case 'Furniture':
            case 'Hogar':
              categoryArray = this.catalogService.furnitureCatalogs;
              break;
              case 'Travel':
                case 'Viajes':
                  categoryArray = this.catalogService.travelCatalogs;
                  break;
                  case 'Beauty':
                    case 'Belleza':
                      categoryArray = this.catalogService.beautyCatalogs;
                      break;
                      case 'Other':
                        case 'Otros':
                          categoryArray = this.catalogService.otherCatalogs;
                          break;
   default:
     break;
 }
this.arrayCategory = categoryArray;

}

// open catalog view modal from home
async openCatViewModal(item) {
  this.likedStateCheck(item);
  const modal = await this.modalController.create({
    component: CatalogViewPage,
    cssClass: 'modalCss',
    mode:'md',

    componentProps: {
      name: item.Name,
      likes: item.Likes,
      id: item.ID,
      startDate: item.StartDate,
      endDate: item.EndDate,
      imageUrl: item.URL,
      country: item.Country,
      pdfPages: item.pdfPages,
      likeState: this.storedLikeSate
    }
  });
  modal.onDidDismiss().then(() => {
    this.loadStoredItems();
    this.admobService.showBanner();
  });
  return await modal.present();
}

 // check the likedState
 likedStateCheck(item) {
   if (this.favCatalogs != null && this.favCatalogs.length > 0) {
    const found = this.favCatalogs.some(el => el.ID === item.ID);
    if (found) {
      this.storedLikeSate = 'like';
    } else {
      this.storedLikeSate = 'unlike';
    }
  } else {
    this.storedLikeSate = 'unlike';
  } 
 
}
// load stored items
loadStoredItems() {
  this.favoriteCatalogsPromise = this.sService.getItems();
  this.favoriteCatalogsPromise.then(i => {
    this.favCatalogs = i;
    });
  }

dismissModal(){
  this.modalController.dismiss();
}


}
