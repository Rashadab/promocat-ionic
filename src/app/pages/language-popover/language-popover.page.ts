import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { LanguageService } from 'src/app/services/language.service';
import { PopoverController } from '@ionic/angular';
@Component({
  selector: 'app-language-popover',
  templateUrl: './language-popover.page.html',
  styleUrls: ['./language-popover.page.scss'],
})
export class LanguagePopoverPage implements OnInit {
 lang: string;
  constructor(  private storage: Storage,
                private lan: LanguageService,
                private popover: PopoverController,
                 ) { }

  ngOnInit() {
  }
  // translate to spanish
 translateSpanish() {
  this.lan.translateSpanish();
  this.storage.get('tutorialComplete').then( data => {
   data.lenguaje = 'es';
   this.storage.set('tutorialComplete', data);
});
  this.popover.dismiss();
 }

 // translate to English
 translateEnglish() {
   this.lan.translateEnglish();
   this.storage.get('tutorialComplete').then( data => {
    data.lenguaje = 'en';
    this.storage.set('tutorialComplete', data);
 });
   this.popover.dismiss();
 }
}
