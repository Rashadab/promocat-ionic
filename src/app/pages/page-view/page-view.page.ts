import { Component, OnInit } from '@angular/core';
import { AdMobFree } from '@ionic-native/admob-free/ngx';
import { AdmobService } from 'src/app/services/admob.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-page-view',
  templateUrl: './page-view.page.html',
  styleUrls: ['./page-view.page.scss'],
})
export class PageViewPage implements OnInit {
  constructor(
      private admobService: AdmobService,
      private modalController: ModalController
  ) { }
url: any;
  slideOpts = {
    zoom: {
      maxRatio: 5,
    }
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.admobService.showInterstitial();
    this.admobService.showBanner();
   }
   ionViewDidLeave() {
   this.admobService.hideBanner();
   }

   dismissModal(){
     this.modalController.dismiss();
   }

}
