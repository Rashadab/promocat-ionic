import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PageViewPageRoutingModule } from './page-view-routing.module';

import { PageViewPage } from './page-view.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PageViewPageRoutingModule
  ],
  declarations: [PageViewPage]
})
export class PageViewPageModule {}
