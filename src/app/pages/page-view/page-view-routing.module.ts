import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageViewPage } from './page-view.page';

const routes: Routes = [
  {
    path: '',
    component: PageViewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageViewPageRoutingModule {}
