import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CountrySelectionPage } from './country-selection.page';

describe('CountrySelectionPage', () => {
  let component: CountrySelectionPage;
  let fixture: ComponentFixture<CountrySelectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountrySelectionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CountrySelectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
