import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NotificationService } from 'src/app/services/notification.service';
import { Router } from '@angular/router';
import { LoadingController, ModalController } from '@ionic/angular';
import { CatalogService } from 'src/app/services/catalog.service';
import { StorageService } from 'src/app/services/storage.service';



@Component({
  selector: 'app-country-selection',
  templateUrl: './country-selection.page.html',
  styleUrls: ['./country-selection.page.scss'],
})
export class CountrySelectionPage implements OnInit {
  country = [
    {
      name: 'Peninsula',
      src: '../../../assets/svgs/Flag_of_Spain.svg'
    },
    {
      name: 'Canarias',
      src: '../../../assets/svgs/Canary_Islands.svg'
    },
  ]
  constructor( private storage: Storage,
               private ns: NotificationService,
               private router: Router,
               private loadingController: LoadingController,
               private modal: ModalController,
               private sService: StorageService,
               private catService: CatalogService
               ) { }

  ngOnInit() {
  }

  loadCountryCatalogs(item){
  this.storage.get('tutorialComplete').then(async data => {

    if (!data ) {
      // subscribe to the topic,
      this.ns.sub(item.name);
     // set storage to the name of the selected country
      data = {
       country: item.name,
       isComplete: true
     };
     // update the storage with the new data.
     this.storage.set('tutorialComplete', data).then(() => {
      console.log('Updated store data to: ', data);
     this.catService.loadCatalogs(data.country);
    }).then(() => {
      this.sService.getCountrySelection('tutorialComplete').then(() => {
       this.router.navigateByUrl('').then(()=> {
        this.presentLoading();
        window.addEventListener('catalogsLoaded', (e) =>{
         setTimeout(() => {
           this.loadingController.dismiss();
           this.modal.dismiss();
         }, 1500);
        })
       })
      })
    });

    } else {
      // if storage isnt empty. i.e if the user is not a first time user
      // check if the selected country is the same as the stored country
     if (data.country === item.name) {
       // if data name is the same as selected item, do nothing, just redirect to home page;
       console.log('Current country is the same as the selected country');
       // navigate to home page
       this.router.navigateByUrl('/');
       // dismiss modal.
       this.modal.dismiss();

     } else {
       // If country name stored is not the same as the selected country name then
       // Unsubscribe from the stored country topic
       this.ns.unsub(data.country);
       // Subscribe to the new country
       this.ns.sub(item.name);
       // update the name of the stored country to the selected country;
       data.country = item.name;
       // update the storage with the new data.
       this.storage.set('tutorialComplete', data).then(() => {
        this.catService.loadCatalogs(data.country);
       }).then(() => {
         this.sService.getCountrySelection('tutorialComplete').then(() => {
          this.router.navigateByUrl('/').then(() => {
            this.presentLoading();
            window.addEventListener('catalogsLoaded', (e) =>{
              setTimeout(() => {
                this.loadingController.dismiss();
                this.modal.dismiss();
              }, 1500);
             })
          })
         })
       });

     }
    }
  });

}

async presentLoading() {
  const loading = await this.loadingController.create({
    spinner: 'bubbles',
    duration: 0,
    showBackdrop: true,
    animated: true,
    cssClass: 'beginLoading',
    mode: 'ios'
  });
  await loading.present();
}


}
