import { StorageService } from 'src/app/services/storage.service';
import { AuthenticationService } from './../../services/authentication.service';
import { CountrySelectionPage } from './../country-selection/country-selection.page';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, PopoverController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Market } from '@ionic-native/market/ngx';
import { LanguagePopoverPage } from '../language-popover/language-popover.page';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
uid: string;
shortId: string;
storedCountry: string;
source: string;
uidLength: number;
countryName: string;
  constructor(
    private modalCtrl: ModalController,
    private socialSharing: SocialSharing,
    private router: Router,
    private authService: AuthenticationService,
    private sService: StorageService,
    private market: Market,
    private popoverController: PopoverController
    ) {   }

  ngOnInit() {
    this.uid = this.authService.userId;
    this.trimUid(this.uid);
    this.countryName = this.sService.countryName;
    console.log(this.countryName);
    this.assignFlag(this.countryName);
  }

    // trim uid
    trimUid(uid){
      if (this.uid) {
        this.uidLength = this.uid.length;
        this.shortId = this.uid.substring(0, this.uidLength / 2);
      }
    }
    // assign flag
    assignFlag(country){
      if (country === 'Peninsula') {
        this.source = '../../../assets/svgs/Flag_of_Spain.svg';
      }
      if (country === 'Canarias') {
        this.source = '../../../assets/svgs/Canary_Islands.svg';

      }

    }

    // Share via Social Media
    shareSocailMedia() {
      this.socialSharing.share('https://play.google.com/store/apps/details?id=io.ionic.promocat&hl=en').then(() => {
      }).catch(e => {
        console.log(e);
      });
    }

    // open app in store to rate us
    rateUs() {
      this.market.open('io.ionic.promocat');
    }

  // ----------------------------------------------Modals ----------------------------------------------
    // select a country
    async countrySelect() {
      this.modalCtrl.dismiss();
      const modal = await this.modalCtrl.create({
          component: CountrySelectionPage,
          cssClass: 'countryCss',
          mode:'md',
        });
      return await modal.present();
      // this.router.navigateByUrl('/country-selection');
        }

    // dismiss modal
    dismissModal() {
      this.modalCtrl.dismiss();
    }

    // add a popover for language
    async presentPopover() {
      const popover = await this.popoverController.create({
        component: LanguagePopoverPage,
        cssClass: 'langClassCss',
        mode:'md',
        translucent: true
      });
      return await popover.present();
    }

    // ----------------------------------------------------------------------------------------------------

}
