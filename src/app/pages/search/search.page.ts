import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CatalogService } from 'src/app/services/catalog.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  allCatalogs: any;
  searchText = '';
  segment = 0;
  brands = [
    {
      name: 'Aldi',
      img :'../assets/images/brands/Aldi.jpg'
    },
    {
      name: 'Carrefour',
      img :'../assets/images/brands/Carrefour.jpg'
    },
    {
      name: 'Consum',
      img :'../assets/images/brands/Consum.jpg'
    },
    {
      name: 'Dia',
      img :'../assets/images/brands/Dia.jpg'
    },
    {
      name: 'El Corte Ingles',
      img :'../assets/images/brands/El Corte Ingles.jpg'
    },
    {
      name: 'Lidl',
      img :'../assets/images/brands/Lidl.jpg'
    },
    {
      name: 'Ikea',
      img :'../assets/images/brands/Ikea.jpg'
    }
  ]
  constructor(
    private catService: CatalogService,
    private modalCtl: ModalController
  ) { }

  ngOnInit() {
    this.allCatalogs= this.catService.allCats;
    console.log(this.allCatalogs);
  }


  segmentChanged($event){
    this.searchText = '';
    this.segment = $event.detail.value;
  }

  dismissModal(){
    this.modalCtl.dismiss();
  }

   // filter brands with chip values
   filterBrand(item){
     this.searchText = item.name;
  }
}
