import { AuthenticationService } from './../../services/authentication.service';
import { CountrySelectionPage } from './../country-selection/country-selection.page';
import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  constructor(private modalCtrl: ModalController, private ns: NotificationService,
              private authService: AuthenticationService) {
  }
isAuthenticated: boolean;

  slideOpts = {
    initialSlide: 0,
    speed: 400,
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
  };

  ngOnInit() {
  }

  // open country select modal
 async openCountrySelect() {
    const modal = await this.modalCtrl.create({
      component: CountrySelectionPage,
      cssClass: 'modalCss'
    });

    return await modal.present();
  }

  // grant permission to send notifications
   grantPermission() {
    this.signInAnonymously().then(() => {
        this.openCountrySelect();
       });
  }

  // sign users in anonymosly
  async signInAnonymously() {
  await this.authService.signInAnonymously();
  }

}
