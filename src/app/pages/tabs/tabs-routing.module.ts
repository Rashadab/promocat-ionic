import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [ 
      {
        path: 'catalogs',
        loadChildren: () => import('../catalogs/catalogs.module').then( m => m.CatalogsPageModule)
      },

      {
        path: 'brands',
        loadChildren: () => import('../brands/brands.module').then( m => m.BrandsPageModule)
      },
      {
        path: 'categories',
        loadChildren: () => import('../categories/categories.module').then( m => m.CategoriesPageModule)
      },
      {
        path: 'products',
        loadChildren: () => import('../products/products.module').then( m => m.ProductsPageModule)
      },
    ]
  }, 

  {
    path: '',
    redirectTo: 'tabs/catalogs',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
