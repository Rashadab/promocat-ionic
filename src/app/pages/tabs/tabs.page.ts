import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CatalogService } from 'src/app/services/catalog.service';
import { SearchPage } from '../search/search.page';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  allCats: any;
  constructor(
               private catService: CatalogService, 
               private modalController: ModalController
  ) { }

  ngOnInit() {
   
  }


  /* ------------------------------------------------Modals ------------------------------------------------------------ */
    // Open Search
    async openSearch(item){
    const modal = await this.modalController.create({
      component: SearchPage,
      cssClass: 'modalCss',
      mode: 'md',
    });
    modal.onDidDismiss().then(() => {
    })
    return await modal.present();

    }


}
