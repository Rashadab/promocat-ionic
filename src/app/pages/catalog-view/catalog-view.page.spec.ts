import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CatalogViewPage } from './catalog-view.page';

describe('CatalogViewPage', () => {
  let component: CatalogViewPage;
  let fixture: ComponentFixture<CatalogViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogViewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CatalogViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
