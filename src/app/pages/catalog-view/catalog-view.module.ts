import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CatalogViewPageRoutingModule } from './catalog-view-routing.module';

import { CatalogViewPage } from './catalog-view.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CatalogViewPageRoutingModule,
    TranslateModule

  ],
  declarations: [CatalogViewPage]
})
export class CatalogViewPageModule {}
