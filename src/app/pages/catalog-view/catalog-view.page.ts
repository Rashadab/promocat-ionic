import { Component, OnInit } from '@angular/core';
import { Catalog } from 'src/app/modals/Catalog';
import { ModalController, ToastController, LoadingController } from '@ionic/angular';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { StorageService } from 'src/app/services/storage.service';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { TranslateService } from '@ngx-translate/core';
import { CatalogService } from 'src/app/services/catalog.service';
import { AdmobService } from 'src/app/services/admob.service';

@Component({
  selector: 'app-catalog-view',
  templateUrl: './catalog-view.page.html',
  styleUrls: ['./catalog-view.page.scss'],
  animations: [
    trigger('heart', [
      state('unlike', style({
        color: '#222428',
        opacity: '1',
        transform: 'scale(1)'
      })),
      state('like', style({
        color: '#e74c3c',
        opacity: '1',
        transform: 'scale(1.1)'
      })),
      transition('unlike <=> like', animate('100ms ease-out'))
    ])
  ]

})
export class CatalogViewPage implements OnInit {
  likeState: any;
  // Original name for the heart icon
  public iconName = 'heart-outline';
  name: any;
  id: any;
  endDate: any;
  startDate: any;
  pdfPages: Array<any>;
  stringStartDate: string;
  stringEndDate: string;
  catalogData = {} as Catalog;
  imageUrl: any;
  country: any;
  numOfLikes: any;
  sliderOpts = {
    zoom: {
      maxRatio: 3,
    },
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
   };

  constructor(
              private modalController: ModalController,
              private storageService: StorageService,
              private launchNavigator: LaunchNavigator,
              private socialSharing: SocialSharing,
              private loadController: LoadingController,
              private catalogService: CatalogService,
              private admobService: AdmobService
              ) { }

  ngOnInit() {
    this.presentLoading();
    this.stringEndDate = new Date(this.endDate.seconds * 1000).toLocaleDateString();
    this.stringStartDate = new Date(this.startDate.seconds*1000).toLocaleDateString();
    this.catalogData.ID = this.id;
    this.catalogData.numOfLikes = this.numOfLikes;
    this.catalogData.LikeState= this.likeState;
    this.heartShape();
  }

  ionViewDidEnter() {
    this.admobService.showInterstitial();
    this.admobService.showBanner();
   }
   ionViewDidLeave() {
   this.admobService.hideBanner();
   }

  // dismiss modal
  dismissModal(){
    this.modalController.dismiss();
  }
  // method to check if the heart is full or empty
  heartShape() {
    if (this.likeState === 'like') {
      this.iconName = 'heart';
    } else {
      this.iconName = 'heart-outline';
    }
  }


  // Like a catalog
  likeCatalog(){
    this.likeState = 'like';
    this.catalogData.LikeState = this.likeState;
    this.incrementLikes();
    this.iconName = 'heart';
    this.storageService.likeCatalog(this.catalogData);
  }

  // Unlike a catalog
  unlikeCatalog() {
    this.likeState = 'unlike';
    this.catalogData.LikeState = this.likeState;
    this.decrementLikes();
    this.iconName = 'heart-outline';
    this.storageService.deleteCatalog(this.catalogData.ID);
  }

  // Toggle the Like State
  toggleLikeState() {

    if (this.likeState === 'unlike') {
      this.likeCatalog();
    } else {
      this.unlikeCatalog();
    }
  }

 

async presentLoading() {
  const loading = await this.loadController.create({
    spinner: 'lines',
    duration: 1000,
    showBackdrop: true,
    animated: true,
    cssClass: 'spinnerCss',
    mode: 'ios'
  });
  await loading.present();
}
  // .......................................Share Social Media.................................................
// Share via Social Media
shareSocailMedia() {
  this.socialSharing.share('https://play.google.com/store/apps/details?id=io.ionic.promocat&hl=en').then(() => {
  }).catch(e => {
    console.log(e);
  });
}


   // open store location in map
   takeMeToLocation(name) {

    this.launchNavigator.navigate(name)
      .then(
        error => console.log('Error launching navigator', error)
      );
  }
  // ......................................... Admob .........................................................


// ..................................... Firestore ...........................................................


  // incremenet like
  incrementLikes() {
    this.catalogService.incrementLikes(this.country,this.id)

  }

  // decrement likes
  decrementLikes() {
    this.catalogService.decrementLikes(this.country,this.id)

  }

  


}
