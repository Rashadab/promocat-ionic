import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatalogViewPage } from './catalog-view.page';

const routes: Routes = [
  {
    path: '',
    component: CatalogViewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatalogViewPageRoutingModule {}
