import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
allcats: any;
  constructor(
    private catalogService: CatalogService
  ) { }

  ngOnInit() {
    this.allcats = this.catalogService.allCats;
  }

}
