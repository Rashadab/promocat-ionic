import { Component } from '@angular/core';
import { LanguageService } from './services/language.service';
import { Platform, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CatalogService } from './services/catalog.service';
import { StorageService } from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private lanService: LanguageService,
    private catalogService: CatalogService,
    private storageService: StorageService,
    private loadingController: LoadingController,
  ) {
    this.lanService.setIntialLanguage();
    this.storageService.getCountrySelection('tutorialComplete').then(async data => {
      if(data){
        await this.catalogService.loadCatalogs(data.country);
      }
    }).then(() => {
      this.initializeApp();

    })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async presentBeginingLoading() {
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      duration: 2200,
      cssClass: 'beginLoading',
    });
    await loading.present();
  }

}
