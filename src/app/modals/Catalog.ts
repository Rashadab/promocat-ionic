export interface Catalog {
    Category?: string,
    Country?: string,
    EndDate?: string,
    ID?: string,
    Likes?: number,
    Name?: string,
    PublishDate?: any,
    StartDate?: any,
    URL?: string,
    pdfPages?: Array<string>,
    publishDateString?: string,
    LikeState?: string;
    storedName?: string;
    endDateTimestamp?: string;
    numOfLikes?:number;

}