
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterProd'
})
export class FilterProductsPipe implements PipeTransform {
  transform(pdfPages: any[], searchText: string, fieldName: string): any[] {

    // return empty array if array is false
    if (!pdfPages) { return []; }

    // return empty array if search text is empty
    if (!searchText) { return []; }

    // convert the searchText to lower case
    searchText = searchText.toLowerCase();

    // retrun the filtered array
    // retrun the filtered array

   return pdfPages.filter(page => {
   if(page && page[fieldName] ){
    return page[fieldName].toLowerCase().includes(searchText);
   }
  })

  }

}
