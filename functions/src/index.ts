import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as vision from '@google-cloud/vision';

admin.initializeApp(functions.config().firebase);
const db = admin.firestore();
const date = new Date();
const visionClient =  new vision.ImageAnnotatorClient();

exports.addFile = functions.storage.object().onFinalize( async object => {
       if (object && object.name) {
        const [countryName, name, category, startDate, endDate] = object.name.split('+');
         let textArray: any;
        const fileBucket = object.bucket;
        const filePath = object.name;
        const textDetectionResult = await visionClient.documentTextDetection(`gs://${fileBucket}/${filePath}`);
        for (const el of textDetectionResult) {
            const detections = el.fullTextAnnotation?.text;
            textArray = detections;
        }    
       
       const modifiedStartDate = new Date(startDate || '').setUTCHours(0, 0, 0, 0 );
        const newStartDate = new Date(modifiedStartDate);

        const modifiedEndDate = new Date(endDate || '').setHours(18, 59 , 5 , 9);
        const newEndDate = new Date(modifiedEndDate);

        // GET THE URL
        const url = object.mediaLink;

        // GET THE ID
        const id = '@' + countryName + '-' + name + '-' + category + '-' + startDate + '-' + endDate;

        const pubDate = date.toUTCString();

        // NUM OF LIKES
        const numOfLikes = 0;

        // DATE OF PUBLISHING THE CATALOG
        const publishDate = date.setHours(0, 0, 0, 0);
        const publishDateTime = new Date(publishDate);

        const pdfPageArray: Array<any> = [];


        
        const dbRef = db.collection('Country').doc(countryName).collection('Catalogs').doc(id);

        return db.runTransaction(async transaction => {
            
                if(!object.contentType?.startsWith('image/')){
                    return await transaction.set(dbRef, {
                     ID: id,
                     Name: name,
                     Likes: numOfLikes,
                     StartDate: newStartDate, // Time Stamp
                     EndDate: newEndDate, // Time Stamp
                     Country: countryName,
                     Category: category,
                     PublishDate: publishDateTime, // Time Stamp
                     publishDateString: pubDate,
                     pdfPages: pdfPageArray,
                     URL: url,
    

                });
                }else {
                    if(object && object.name){
                        const index = object.name.lastIndexOf("/");  // position of last "/"
                        const fileName = object.name.substring(index + 1);  // will be "1.jpeg"
                        const psIndex = fileName.indexOf(".");   // position of "."
                        const fileNumber = fileName.substring(0, psIndex);  // everything up to the "."


                    console.log(object);
                    return transaction.get(dbRef).then(async doc => {
                    const myDoc = doc.data();

                    if(myDoc){
                        console.log('fileNumber: ', fileNumber)
                        if(fileNumber === '000'){
                            console.log(fileNumber)
                            return await transaction.update(dbRef, {URL: url, sort_number: fileNumber});

                        }
                        return await transaction.update(dbRef, {pdfPages: myDoc.pdfPages.concat({
                            url:url,
                            sort_number: fileNumber,
                            text: textArray,
                            name: name
                        })});

                    } else {
                        return null;    
                    }
                    })

                    }

                    return;
                }
            
        });

    }
       return;

}
);
